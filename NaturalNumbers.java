import java.util.Scanner;
public class NaturalNumbers {
	
			public static void main(String[] args) {
				int num,f,sum = 0;

				Scanner scan = new Scanner(System.in);
				{
					System.out.println("Enter Your Number:");
					num = scan.nextInt();
					
					for(f = 1; f <= num; f++)
					{
						sum = sum + f; 
					}	
					System.out.println("Sum of Natural Numbers from 1 to "+ num + " is " + sum);
}
}
}