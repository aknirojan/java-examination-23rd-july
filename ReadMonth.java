import java.util.Scanner;

public class ReadMonth {
	
		public static void main(String[] args) {
			int num;
			Scanner scan = new Scanner(System.in);
			{
				System.out.println("Enter the number:");
				num= scan.nextInt();
				
			switch (num) {
			case 1:
				System.out.println("January\tAnd It Has 31 Days");
				break;
			case 2:
				System.out.println("February\tAnd It Has 28 Days");
				break;
			case 3:
				System.out.println("March\tAnd It Has 30 Days");
				break;
			case 4:
				System.out.println("April\tAnd It Has 31 Days");
				break;
			case 5:
				System.out.println("May\tAnd It Has 30 Days");
				break;
			case 6:
				System.out.println("June\tAnd It Has 31 Days");
				break;
			case 7:
				System.out.println("July\tAnd It Has 30 Days");
				break;
			case 8:
				System.out.println("August\tAnd It Has 31 Days");
				break;
			case 9:
				System.out.println("September\tAnd It Has 30 Days");
				break;
			case 10:
				System.out.println("October\tAnd It Has 31 Days");
				break;
			case 11:
				System.out.println("November\tAnd It Has 30 Days");
				break;
			case 12:
				System.out.println("December\tAnd It Has 31 Days");
				break;
			default:
				System.out.println("Invalid Month");
			}
		}
	}
}	

